import commonReducer from './common/reducer'
import restReducer from './rest/reducer'
import configReducer from './config/reducer'
import {combineReducers} from "redux";
export default combineReducers({
  common: commonReducer,
  rest: restReducer,
  config: configReducer
})
