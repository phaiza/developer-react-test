import styled, { css } from 'styled-components'
import {FormControl} from 'react-bootstrap'
import React from 'react';
const StyledComponent = styled(FormControl)`
  font-size:20px;
  border-radius:0;
  padding: 0 1.2em;
  height:2.5em;
`;
export default StyledComponent
