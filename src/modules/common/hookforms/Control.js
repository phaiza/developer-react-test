import React from 'react';
import styled from 'styled-components'
import Error from './Error'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'
const Wrapper = styled.div`
  margin-bottom: 10px;
  .tooltip-icon {
    margin-left: 10px;
  }
`
const Control = ({name, label, placeholder, className, required, children, error, patternLabel, min, max, tooltip}) => {
  className = className || ''
  label = label || ''
  return (<Wrapper className={className}>
    <label htmlFor={name}>{label}
      {
        tooltip &&
        <OverlayTrigger overlay={<Tooltip id='tooltip-disabled'>{tooltip}</Tooltip>}>
          <FontAwesomeIcon icon={['fas','question-circle']} className='tooltip-icon'/>
        </OverlayTrigger>
      }
    </label>
    {children}
    <Error label={label} error={error} patternLabel={patternLabel} min={min} max={max}/>
  </Wrapper>)
}
export default Control
