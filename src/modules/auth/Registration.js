import React, { useState, useRef } from "react";
import { Link } from "react-router-dom";
import { authRequirements } from "../common/constants";
import Title from "../common/components/Title";
import Input from "../common/hookforms/Input";
import Email from "../common/hookforms/Email";
import Submit from "../common/hookforms/Submit";
import Password from "../common/hookforms/Password";
import CenteredScreen from "../layout/CenteredScreen";
import {
  Form,
  Modal,
  Button,
  Accordion,
  Tabs,
  Tab,
  Collapse,
} from "react-bootstrap";

import useForm from "react-hook-form";
import styled from "styled-components";
import AuthBanner from "./AuthBanner";
const Wrapper = styled(CenteredScreen)`
  .auth-banner {
    width: 325px;
    margin-bottom: 15px;
    @media (min-width: 768px) {
      width: 400px;
    }
  }
  form {
    width: 300px;
    text-align: left;
    margin: 0 auto 15px;
    .submit {
      margin-top: 42px;
      text-align: center;
    }
  }
`;
const Registration = ({ location, history }) => {
  const form = useForm();
  const [show, setShow] = useState(false);
  const [checked, setChecked] = useState(false);
  const [agreed, setAgree] = useState(false);
  const [open, setOpen] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleChange = (e) => {
    setChecked(!checked);
    console.log(checked);
    if (!checked) {
      setAgree(true);
    } else setAgree(false);
  };
  const onSubmit = (data) => {
    console.log(data);
    handleShow();
  };

  const onlyAlpha = /^\w+$/;
  return (
    <Wrapper className="screen-login">
      <Title title="Log in" />
      <div className="container-fluid">
        <AuthBanner />
        <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
          <Input
            name="username"
            form={form}
            label="Username"
            minLength={authRequirements.USERNAME_MIN}
            maxLength={authRequirements.USERNAME_MAX}
            pattern={onlyAlpha}
            patternLabel="Must be only a-z 0-9"
            required
          />
          <Email name="email" form={form} label="Email" required />
          <Password name="password" form={form} label="Password" required />
          <p>
            By clicking Create Account you agree to our{" "}
            <Link to="/terms">Terms</Link> and{" "}
            <Link to="/privacy">Privacy Policy</Link>
          </p>
          <Submit
            className="submit"
            label="Create account"
            form={form}
            primary
          />
        </Form>
        <p>
          Already have an account? <Link to="login">Sign in now</Link>
        </p>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
        dialogClassName="registration-terms-modal"
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">
            Terms & Privacy Policy
          </Modal.Title>
        </Modal.Header>
        <Modal.Body
          style={{ "max-height": "calc(100vh - 210px)", "overflow-y": "auto" }}
        >
          <Button
            onClick={() => setOpen(!open)}
            aria-controls="example-collapse-text"
            aria-expanded={open}
            className="btn-collapse"
          >
            Statement of Intent
          </Button>
          <Collapse in={open}>
            <div id="example-collapse-text">
              Anim pariatur cliche reprehenderit, enim eiusmod high life
              accusamus terry richardson ad squid. Nihil anim keffiyeh
              helvetica, craft beer labore wes anderson cred nesciunt sapiente
              ea proident.
            </div>
          </Collapse>
          <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
            <Tab eventKey={1} title="Terms of Use">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Maecenas fermentum posuere diam, ut ullamcorper sem pellentesque
                bibendum. Donec sed felis id est luctus iaculis. Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. Integer id metus
                venenatis, sagittis lorem sed, tempus neque.{" "}
              </p>
              <p>
                {" "}
                Suspendisse at tellus eu purus egestas pharetra vel non urna.
                Vestibulum feugiat urna quis mi facilisis mollis. Phasellus ut
                blandit nunc, at mattis libero. Curabitur iaculis fermentum mi,
                fermentum ultricies sem posuere non. Vivamus eu dui eu orci
                rhoncus ultricies in in nisi.{" "}
              </p>
              <p>
                Etiam euismod in leo eu iaculis. Suspendisse est ex, ullamcorper
                at erat ut, hendrerit egestas dui. Cras vestibulum vel leo nec
                bibendum. Vivamus gravida eget augue vel fringilla. Nunc
                pellentesque in ligula vel dictum. Nam facilisis ut nisl a
                eleifend. Sed vitae tempor sem.
              </p>
              <p>
                Sed ac libero nulla. Quisque accumsan egestas turpis nec
                tincidunt. Aenean sollicitudin in nunc vel blandit. Integer
                scelerisque ligula et nunc finibus porttitor. Etiam porta, lacus
                ac volutpat dignissim, sem nibh luctus nibh, vitae cursus nisi
                dolor at sem. Duis id suscipit sapien. Curabitur tempor lobortis
                tellus, ut tempus purus sagittis nec. Sed lorem purus, molestie
                non purus quis, tincidunt volutpat arcu. Maecenas blandit ex vel
                nulla consequat fringilla sed sed velit. Ut vel imperdiet nunc.
                Integer velit urna, consequat et libero nec, accumsan venenatis
                magna. Nunc imperdiet leo hendrerit commodo lobortis. Phasellus
                ut massa eros.
              </p>
              <p>
                Fusce sapien est, rutrum id vestibulum non, imperdiet quis
                turpis. In ultricies odio ipsum, et congue sem ultrices non.
                Praesent rhoncus justo id pulvinar semper. Nulla sed semper
                odio, a convallis libero. Pellentesque neque metus, tincidunt et
                venenatis ac, hendrerit eu ex. In hac habitasse platea dictumst.
                Morbi ac nisl nulla. Sed ipsum nisl, rhoncus quis pellentesque
                eu, vestibulum ac sapien.
              </p>
            </Tab>
            <Tab eventKey={2} title="Privacy Policy">
              <p>
                Mauris metus enim, rhoncus vitae risus et, porttitor euismod
                ipsum. Nunc aliquam faucibus massa, sit amet venenatis turpis
                finibus ut. Suspendisse eu facilisis arcu. Nullam viverra
                tincidunt magna, et pretium augue cursus ac. Aenean posuere nunc
                quam. Sed et velit nisi. Aliquam ullamcorper purus non feugiat
                viverra. Cras aliquet nisi in nulla ullamcorper mattis. Nullam
                in sem eget erat auctor laoreet. Ut quis consectetur orci, in
                pellentesque diam. Vivamus mattis vulputate nibh in blandit.
              </p>
              <p>
                Morbi eros augue, tincidunt sed nisi vel, fringilla tempus
                justo. Pellentesque facilisis neque felis, non porttitor nisi
                gravida id. Pellentesque feugiat, mi id fringilla iaculis, ante
                quam porttitor diam, a convallis purus mauris id dui. Morbi non
                ligula nulla. Donec pharetra arcu est, in scelerisque elit
                rhoncus quis. Quisque ultricies orci in ornare condimentum.
                Praesent non laoreet nisi. Pellentesque eget nunc at ex
                hendrerit suscipit. Ut aliquam ornare purus lacinia volutpat. In
                interdum nisi eget posuere maximus. Donec nisi odio, tempor
                vitae ornare vel, pulvinar eget mauris. Suspendisse sed purus
                tristique, eleifend eros fringilla, maximus libero. Donec sed
                condimentum elit, et dapibus arcu.
              </p>
            </Tab>
          </Tabs>
        </Modal.Body>
        <Modal.Footer>
          <input
            type="checkbox"
            checked={checked ? "checked" : ""}
            onChange={handleChange}
          />{" "}
          <span>I agree to the privacy policy & terms and conditions.</span>
          <br />
          <Button
            variant="secondary"
            onClick={handleClose}
            className="btn-agree"
          >
            Cancel
          </Button>
          <Button
            variant="primary"
            onClick={handleClose}
            disabled={!agreed ? "disabled" : ""}
            className="btn-agree"
          >
            Accept
          </Button>
        </Modal.Footer>
      </Modal>
    </Wrapper>
  );
};
export default Registration;
